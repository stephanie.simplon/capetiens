var express = require('express');
var fs = require('fs');
var htmlspecialchars = require('htmlspecialchars');

var app = express();
var port = 8080;

function removeExtraSpace(str) {
    return str.replace(/[\s]{2,}/g, " ").trim(); // Enlève les espaces multiples 
}

app.set ('view-engine', 'ejs') ;

app.use(express.static('public'));
app.use(express.urlencoded());
app.use(express.json());

app.get('/', function(req, res) {
    res.render('index.ejs');
});

app.get('/capet/', function(req, res) {
    res.render('capet.ejs');
});

app.get('/auguste/', function(req, res) {
    res.render('auguste.ejs');
});

app.get('/louisix/', function(req, res) {
    res.render('louisix.ejs');
});

app.get('/philippeiv/', function(req, res) {
    res.render('philippeiv.ejs');
});

app.get('/form/', function(req, res) {
    let message = "";
    res.render('form.ejs', {message: message});
});

app.post('/form/', function(req, res) {
    // récupération des données issues du formulaire et passées en POST
    console.log(req.body);
    const dataForm = req.body;
    dataForm.userName = removeExtraSpace(htmlspecialchars(dataForm.userName));
    dataForm.userFirstName = removeExtraSpace(htmlspecialchars(dataForm.userFirstName));
    dataForm.userBirthday = removeExtraSpace(htmlspecialchars(dataForm.userBirthday));
    dataForm.userAddress = removeExtraSpace(htmlspecialchars(dataForm.userAddress));
    dataForm.userZipCode = removeExtraSpace(htmlspecialchars(dataForm.userZipCode));
    dataForm.userCity = removeExtraSpace(htmlspecialchars(dataForm.userCity));
    dataForm.userMail = removeExtraSpace(htmlspecialchars(dataForm.userMail));
    dataForm.userPhone = removeExtraSpace(htmlspecialchars(dataForm.userPhone));
    // vérification du numéro de téléphone avec une expression régulière
    const regexPhone = /[0-9]{10}/;
    let message = '';
    if (regexPhone.test(dataForm.userPhone) !== true) {
        message += "Le numéro de téléphone est incorrect.";
    }
    const regexZipCode = /[0-9]{5}/;
    if (regexZipCode.test(dataForm.userZipCode) !== true) {
        message += "Le code postal est incorrect.";
    }
    const regexMail = /[a-z0-9\-_]+[a-z0-9\.\-_]*@[a-z0-9\-_]{2,}\.[a-z\.\-_]+[a-z\-_]+/;
    if (regexMail.test(dataForm.userMail) !== true) {
        message += "L'adresse e-mail n'est pas valide.";
    }
    const regexDate = /[0-9]{4}[-/]{1}[01][0-9]{1}[-/]{1}[0123][0-9]{1}/;
    if (regexDate.test(dataForm.userBirthday) !== true) {
        message += "La date de naissance n'est pas valide.";
    }
    if (message === ''){
        // écriture dans le fichier base.json
        const data = JSON.stringify(dataForm, null, 2);
        fs.appendFile('base.json', data, (err) => {
            if(err) throw err;
        });
    }
    
    res.render('form.ejs', {message: message});
});

app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.status(404).send('Page introuvable !');
});

app.listen(port);


