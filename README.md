Site internet ayant pour but la diffusion d'informations sur les rois de la dynastie capétienne.

Installation
npm install

Démarrage
node serveur.js

Accès
localhost:8080

Fichier base de données
./base.json