let i = 1;
let nbrePhotos = 8;

function diapo(choix) {
    img = document.getElementById("img1");
    switch (choix) {
        case "0":
            img.src = i + '.png';
            afficheNom();
            myVar = setInterval(function () {
                i++;
                if (i > nbrePhotos) { i = 1; }
                afficheNom();
                img.src = i + '.png'
            }
                , 2000);
            break;
        case "1":
            clearInterval(myVar);
            myVar = setInterval(function () {
                i++;
                if (i > nbrePhotos) { i = 1; }
                afficheNom();
                img.src = i + '.png'
            }
                , 2000);
            break;
        case "2":
            clearInterval(myVar);
            i++;
            if (i > nbrePhotos) { i = 1; }
            img.src = i + '.png';
            afficheNom();
            break;
        case "3":
            clearInterval(myVar);
            i--;
            if (i < 1) { i = nbrePhotos; }
            img.src = i + '.png';
            afficheNom();
            break;
        case "4":
            clearInterval(myVar);
            break;
    }
}

function afficheNom() {
    switch (i) {
        case 1:
        case 2:
            nom = "Hugues Capet";
            break;
        case 3:
        case 4:
            nom = "Philippe Auguste";
            break;
        case 5:
        case 6:
            nom = "Louis IX";
            break;
        case 7:
        case 8:
            nom = "Philippe IV";
            break;
    }
    document.getElementById("nomDuRoi").innerHTML = '<p>' + nom + '</p>';
}

function horloge(){
    setInterval(function() {
    heure = new Date();
    if (heure.getMinutes()%2) { //minutes impaires
        document.getElementById("clock").innerHTML = '<div style="color: blue;">' + heure.toLocaleTimeString() + '</div>';
    } else {
        document.getElementById("clock").innerHTML = '<div style="color: green;">' + heure.toLocaleTimeString() + '</div>';
    }} , 1000)
}

function activeRubrique(nom) {
    let rubriques = document.getElementsByClassName('rubrique');
    switch (nom) {
        case "index" :
            rubriques[0].style.backgroundColor = '#e9eaed';
            rubriques[6].style.backgroundColor = '#e9eaed';
            break;
        case "capet" :
            rubriques[1].style.backgroundColor = '#e9eaed';
            rubriques[7].style.backgroundColor = '#e9eaed';
            break;
        case "auguste" :
            rubriques[2].style.backgroundColor = '#e9eaed';
            rubriques[8].style.backgroundColor = '#e9eaed';
            break;
        case "louisix" :
            rubriques[3].style.backgroundColor = '#e9eaed';
            rubriques[9].style.backgroundColor = '#e9eaed';
            break;
        case "lebel" :
            rubriques[4].style.backgroundColor = '#e9eaed';
            rubriques[10].style.backgroundColor = '#e9eaed';
            break;
        case "form" :
            rubriques[5].style.backgroundColor = '#e9eaed';
            rubriques[11].style.backgroundColor = '#e9eaed';
            break;
    }
}

function openBurger() {
    var x = document.getElementById("liens");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
      x.style.display = "block";
    }
}